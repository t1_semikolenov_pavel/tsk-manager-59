package ru.t1.semikolenov.tm.service.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.t1.semikolenov.tm.api.repository.dto.IUserDtoOwnedRepository;
import ru.t1.semikolenov.tm.api.service.dto.IUserDtoOwnedService;
import ru.t1.semikolenov.tm.dto.model.AbstractUserDtoOwnedModel;
import ru.t1.semikolenov.tm.enumerated.Sort;
import ru.t1.semikolenov.tm.enumerated.Status;
import ru.t1.semikolenov.tm.exception.field.EmptyDescriptionException;
import ru.t1.semikolenov.tm.exception.field.EmptyIdException;
import ru.t1.semikolenov.tm.exception.field.EmptyNameException;
import ru.t1.semikolenov.tm.exception.field.EmptyUserIdException;
import ru.t1.semikolenov.tm.exception.field.EmptyStatusException;

import javax.persistence.EntityManager;
import javax.persistence.EntityNotFoundException;
import java.util.Comparator;
import java.util.List;

@Service
public abstract class AbstractUserDtoOwnedService<M extends AbstractUserDtoOwnedModel, R extends IUserDtoOwnedRepository<M>>
        extends AbstractDtoService<M, R>
        implements IUserDtoOwnedService<M> {

    @NotNull
    @Override
    protected abstract IUserDtoOwnedRepository<M> getRepository();

    @Override
    @Transactional
    public void clear(@NotNull final String userId) {
        if (userId.isEmpty()) throw new EmptyUserIdException();
        @NotNull final IUserDtoOwnedRepository<M> repository = getRepository();
        repository.clear(userId);
    }

    @NotNull
    @Override
    public List<M> findAll(@NotNull final String userId) {
        if (userId.isEmpty()) throw new EmptyUserIdException();
        @NotNull final IUserDtoOwnedRepository<M> repository = getRepository();
        return repository.findAll(userId);
    }

    @NotNull
    @Override
    public List<M> findAll(@NotNull final String userId, @Nullable final Comparator<M> comparator) {
        if (userId.isEmpty()) throw new EmptyUserIdException();
        if (comparator == null) return findAll(userId);
        @NotNull final IUserDtoOwnedRepository<M> repository = getRepository();
        return repository.findAll(userId, comparator);
    }

    @NotNull
    @Override
    public List<M> findAll(@NotNull final String userId, @Nullable final Sort sort) {
        if (userId.isEmpty()) throw new EmptyUserIdException();
        if (sort == null) return findAll(userId);
        @NotNull final IUserDtoOwnedRepository<M> repository = getRepository();
        return repository.findAll(userId, sort.getComparator());
    }

    @Override
    public boolean existsById(@NotNull final String userId, @NotNull final String id) {
        if (userId.isEmpty()) throw new EmptyUserIdException();
        if (id.isEmpty()) throw new EmptyIdException();
        @NotNull final IUserDtoOwnedRepository<M> repository = getRepository();
        return repository.existsById(userId, id);
    }

    @Nullable
    @Override
    public M findOneById(@NotNull final String userId, @NotNull final String id) {
        if (userId.isEmpty()) throw new EmptyUserIdException();
        if (id.isEmpty()) throw new EmptyIdException();
        @NotNull final IUserDtoOwnedRepository<M> repository = getRepository();
        return repository.findOneById(userId, id);
    }

    @Override
    @Transactional
    public void remove(@NotNull final String userId, @NotNull final M model) {
        if (userId.isEmpty()) throw new EmptyUserIdException();
        @NotNull final IUserDtoOwnedRepository<M> repository = getRepository();
        repository.remove(userId, model);
    }

    @Override
    @Transactional
    public void removeById(@NotNull final String userId, @NotNull final String id) {
        if (userId.isEmpty()) throw new EmptyUserIdException();
        if (id.isEmpty()) throw new EmptyIdException();
        @NotNull final IUserDtoOwnedRepository<M> repository = getRepository();
        repository.removeById(userId, id);
    }

    @Override
    @Transactional
    public void update(@NotNull final M model) {
        @NotNull final IUserDtoOwnedRepository<M> repository = getRepository();
        repository.update(model);
    }

    @Nullable
    @Override
    @Transactional
    public M updateById(
            @Nullable final String userId,
            @Nullable final String id,
            @Nullable final String name,
            @Nullable final String description
    ) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        if (description == null || description.isEmpty()) throw new EmptyDescriptionException();
        @NotNull final IUserDtoOwnedRepository<M> repository = getRepository();
        @Nullable final M model = repository.findOneById(userId, id);
        if (model == null) throw new EntityNotFoundException();
        model.setName(name);
        model.setDescription(description);
        repository.update(model);
        return model;
    }

    @Nullable
    @Override
    @Transactional
    public M changeStatusById(@Nullable final String userId, @Nullable final String id, @Nullable final Status status) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        if (status == null) throw new EmptyStatusException();
        @NotNull final IUserDtoOwnedRepository<M> repository = getRepository();
        @Nullable final M model = repository.findOneById(userId, id);
        if (model == null) throw new EntityNotFoundException();
        model.setStatus(status);
        repository.update(model);
        return model;
    }

    @Override
    public long getCount(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        @NotNull final IUserDtoOwnedRepository<M> repository = getRepository();
        return repository.getCount(userId);
    }

}
