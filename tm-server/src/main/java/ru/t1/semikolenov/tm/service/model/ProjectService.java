package ru.t1.semikolenov.tm.service.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.t1.semikolenov.tm.api.repository.model.IProjectRepository;
import ru.t1.semikolenov.tm.api.service.model.IProjectService;
import ru.t1.semikolenov.tm.exception.field.EmptyDescriptionException;
import ru.t1.semikolenov.tm.exception.field.EmptyNameException;
import ru.t1.semikolenov.tm.exception.field.EmptyUserIdException;
import ru.t1.semikolenov.tm.model.Project;

import java.util.Date;

@Service
public class ProjectService extends AbstractUserOwnedService<Project, IProjectRepository>
        implements IProjectService {

    @NotNull
    @Autowired
    private IProjectRepository repository;

    @NotNull
    @Override
    protected IProjectRepository getRepository() {
        return repository;
    }

    @Nullable
    @Override
    @Transactional
    public Project create(@Nullable final String userId, @Nullable final String name) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        @Nullable Project project = new Project();
        project.setName(name);
        project.setUser(getUserRepository().findOneById(userId));
        @NotNull final IProjectRepository repository = getRepository();
        repository.add(project);
        return project;
    }

    @Nullable
    @Override
    @Transactional
    public Project create(
            @Nullable final String userId,
            @Nullable final String name,
            @Nullable final String description
    ) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        if (description == null || description.isEmpty()) throw new EmptyDescriptionException();
        @Nullable Project project = new Project();
        project.setName(name);
        project.setDescription(description);
        project.setUser(getUserRepository().findOneById(userId));
        @NotNull final IProjectRepository repository = getRepository();
        repository.add(project);
        return project;
    }

    @Nullable
    @Override
    @Transactional
    public Project create(
            @Nullable final String userId,
            @Nullable final String name,
            @Nullable final String description,
            @Nullable final Date dateBegin,
            @Nullable final Date dateEnd
    ) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        if (description == null || description.isEmpty()) throw new EmptyDescriptionException();
        @Nullable Project project = new Project();
        project.setName(name);
        project.setDescription(description);
        project.setUser(getUserRepository().findOneById(userId));
        project.setDateBegin(dateBegin);
        project.setDateEnd(dateEnd);
        @NotNull final IProjectRepository repository = getRepository();
        repository.add(project);
        return project;
    }

}
