package ru.t1.semikolenov.tm.service.dto;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.t1.semikolenov.tm.api.repository.dto.IUserDtoRepository;
import ru.t1.semikolenov.tm.api.service.IPropertyService;
import ru.t1.semikolenov.tm.api.service.dto.IUserDtoService;
import ru.t1.semikolenov.tm.dto.model.UserDTO;
import ru.t1.semikolenov.tm.enumerated.Role;
import ru.t1.semikolenov.tm.exception.entity.UserNotFoundException;
import ru.t1.semikolenov.tm.exception.field.*;
import ru.t1.semikolenov.tm.util.HashUtil;

import javax.persistence.EntityManager;
import java.util.Optional;

@Service
public class UserDtoService extends AbstractDtoService<UserDTO, IUserDtoRepository> implements IUserDtoService {

    @NotNull
    @Autowired
    private IPropertyService propertyService;

    @NotNull
    @Autowired
    private IUserDtoRepository repository;

    @NotNull
    @Override
    protected IUserDtoRepository getRepository() {
        return repository;
    }

    @NotNull
    @Override
    @Transactional
    public UserDTO create(@NotNull final String login, @NotNull final String password) {
        if (login.isEmpty()) throw new EmptyLoginException();
        if (isLoginExist(login)) throw new ExistsLoginException();
        if (password.isEmpty()) throw new EmptyPasswordException();
        @NotNull final IUserDtoRepository repository = getRepository();
        @NotNull final UserDTO user = new UserDTO();
        user.setLogin(login);
        @NotNull final String secret = propertyService.getPasswordSecret();
        @NotNull final Integer iteration = propertyService.getPasswordIteration();
        user.setPasswordHash(HashUtil.salt(password, secret, iteration));
        repository.add(user);
        return user;
    }

    @NotNull
    @Override
    @Transactional
    public UserDTO create(
            @NotNull final String login,
            @NotNull final String password,
            @Nullable final String email
    ) {
        if (login.isEmpty()) throw new EmptyLoginException();
        if (isLoginExist(login)) throw new ExistsLoginException();
        if (password.isEmpty()) throw new EmptyPasswordException();
        if (isEmailExist(email)) throw new ExistsEmailException();
        @NotNull final IUserDtoRepository repository = getRepository();
        @NotNull final UserDTO user = new UserDTO();
        user.setLogin(login);
        @NotNull final String secret = propertyService.getPasswordSecret();
        @NotNull final Integer iteration = propertyService.getPasswordIteration();
        user.setPasswordHash(HashUtil.salt(password, secret, iteration));
        user.setEmail(email);
        repository.add(user);
        return user;
    }

    @NotNull
    @Override
    @Transactional
    public UserDTO create(
            @NotNull final String login,
            @NotNull final String password,
            @Nullable final Role role
    ) {
        if (login.isEmpty()) throw new EmptyLoginException();
        if (isLoginExist(login)) throw new ExistsLoginException();
        if (password.isEmpty()) throw new EmptyPasswordException();
        @NotNull final IUserDtoRepository repository = getRepository();
        @NotNull final UserDTO user = new UserDTO();
        user.setLogin(login);
        @NotNull final String secret = propertyService.getPasswordSecret();
        @NotNull final Integer iteration = propertyService.getPasswordIteration();
        user.setPasswordHash(HashUtil.salt(password, secret, iteration));
        if (role == null) user.setRole(Role.USUAL);
        else user.setRole(role);
        repository.add(user);
        return user;
    }

    @Nullable
    @Override
    public UserDTO findByLogin(@NotNull final String login) {
        if (login.isEmpty()) throw new EmptyLoginException();
        @NotNull final IUserDtoRepository repository = getRepository();
        return repository.findOneByLogin(login);
    }

    @Nullable
    @Override
    public UserDTO findByEmail(@NotNull final String email) {
        if (email.isEmpty()) throw new EmptyEmailException();
        @NotNull final IUserDtoRepository repository = getRepository();
        return repository.findOneByEmail(email);
    }

    @Override
    @Transactional
    public void removeByLogin(@NotNull final String login) {
        if (login.isEmpty()) throw new EmptyLoginException();
        @Nullable final UserDTO user = findByLogin(login);
        if (user == null) throw new UserNotFoundException();
        remove(user);
    }

    @NotNull
    @Override
    @Transactional
    public UserDTO setPassword(@NotNull final String id, @NotNull final String password) {
        if (id.isEmpty()) throw new EmptyIdException();
        if (password.isEmpty()) throw new EmptyPasswordException();
        @NotNull final UserDTO user = findOneById(id);
        @NotNull final String secret = propertyService.getPasswordSecret();
        @NotNull final Integer iteration = propertyService.getPasswordIteration();
        user.setPasswordHash(HashUtil.salt(password, secret, iteration));
        update(user);
        return user;
    }

    @NotNull
    @Override
    @Transactional
    public UserDTO updateUser(
            @NotNull final String id,
            @Nullable final String firstName,
            @Nullable final String lastName,
            @Nullable final String middleName
    ) {
        if (id.isEmpty()) throw new EmptyIdException();
        @NotNull final UserDTO user = findOneById(id);
        user.setFirstName(firstName);
        user.setLastName(lastName);
        user.setMiddleName(middleName);
        update(user);
        return user;
    }

    @NotNull
    @Override
    public Boolean isLoginExist(@Nullable final String login) {
        if (login == null || login.isEmpty()) return false;
        return findByLogin(login) != null;
    }

    @NotNull
    @Override
    public Boolean isEmailExist(@Nullable final String email) {
        if (email == null || email.isEmpty()) return false;
        return findByEmail(email) != null;
    }

    @Override
    @Transactional
    public void lockUserByLogin(@NotNull final String login) {
        if (login.isEmpty()) throw new EmptyLoginException();
        final UserDTO user = Optional.ofNullable(findByLogin(login))
                .orElseThrow(UserNotFoundException::new);
        user.setLocked(true);
        update(user);
    }

    @Override
    @Transactional
    public void unlockUserByLogin(@NotNull final String login) {
        if (login.isEmpty()) throw new EmptyLoginException();
        final UserDTO user = Optional.ofNullable(findByLogin(login))
                .orElseThrow(UserNotFoundException::new);
        user.setLocked(false);
        update(user);
    }

}
